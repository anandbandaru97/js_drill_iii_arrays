// const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'
// function flatten(elements) {
//     // Flattens a nested array (the nesting can be to any depth).
//     // Hint: You can solve this using recursion.
//     // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
// }

const flatten = (elements, depth = 1) => {
    let currentDepth = 0;
    let newArray = [];
    for (let index = 0; index < elements.length; index++) {
        let arrayElement = elements[index];
        if (Array.isArray(arrayElement) && currentDepth < depth) {
            newArray = newArray.concat(flatten(arrayElement, depth - 1));
        } else {
            if (arrayElement != undefined) {
                newArray.push(arrayElement);
            }

        }
    }
    return newArray;
};

module.exports = flatten;