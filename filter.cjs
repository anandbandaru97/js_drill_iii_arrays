// function filter(elements, cb) {
//     // Do NOT use .filter, to complete this function.
//     // Similar to `find` but you will return an array of all elements that passed the truth test
//     // Return an empty array if no elements pass the truth test
// }

const filter = (elements, cb) => {
    if (!Array.isArray(elements)) {
        return 'Invalid Array';
    } else if (!elements.length > 0) {
        return 'Empty Array';
    }
    let evenNumbers = [];
    for (let index = 0; index < elements.length; index++) {
        let arrayElement = elements[index];
        if (cb(arrayElement, index, elements) == true) {
            evenNumbers.push(arrayElement);
        }
    }
    return evenNumbers;
};

module.exports = filter;