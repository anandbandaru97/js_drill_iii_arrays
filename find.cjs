// function find(elements, cb) {
//     // Do NOT use .includes, to complete this function.
//     // Look through each value in `elements` and pass each element to `cb`.
//     // If `cb` returns `true` then return that element.
//     // Return `undefined` if no elements pass the truth test.
// }

const find = (elements, cb) => {
    if (!Array.isArray(elements)) {
        return 'Invalid Array';
    } else if (!elements.length > 0) {
        return 'Empty Array';
    }
    for (let index = 0; index < elements.length; index++) {
        let arrayElement = elements[index];
        if (cb(arrayElement)) {
            return arrayElement;
        }
    }
    return undefined;
};

module.exports = find;