const each = require('../each.cjs');

const cb = (element, index) => {
    console.log(`Iteration ${index}: Modification at ${element}.`);
};

let elements = ['1', '2', '3'];

each(elements, cb);