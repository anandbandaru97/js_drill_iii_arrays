const reduce = require('../reduce.cjs');

const cb = (startingValue, arrayElement) => {
    startingValue += arrayElement;
    return startingValue;
};

let elements = [1, 2, 3];
let startingValue;

console.log(reduce(elements, cb, startingValue));