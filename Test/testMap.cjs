const map = require('../map.cjs');

const cb = (arrayElement) => {
    return arrayElement ** 2;
}

let elements = [1, 2, 3];

console.log(map(elements, cb));