// function map(elements, cb) {
//     // Do NOT use .map, to complete this function.
//     // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
//     // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
//     // Return the new array.
// }

const map = (elements, cb) => {
    if (!Array.isArray(elements)) {
        return 'Invalid Array';
    } else if (!elements.length > 0) {
        return 'Empty Array';
    }
    let mappedArray = [];
    for (let index = 0; index < elements.length; index++) {
        let arrayElement = elements[index];
        mappedArray.push(cb(arrayElement, index, elements));
    }
    return mappedArray;
}

module.exports = map;