// function each(elements, cb) {
//     Do NOT use forEach to complete this function.
//     Iterates over a list of elements, yielding each in turn to the `cb` function.
//     This only needs to work with arrays.
//     You should also pass the index into `cb` as the second argument
//     based off http://underscorejs.org/#each
// }

const each = (elements, cb) => {
    if (!Array.isArray(elements)) {
        return console.log('Invalid Array');
    } else if (!elements.length > 0) {
        return console.log('Empty Array');
    }
    for (let index = 0; index < elements.length; index++) {
        let element = elements[index];
        cb(element, index);
    }
};

module.exports = each;